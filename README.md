# Huffman Codes

Huffman codes are used to create optimal character encodings for a specific character file; this encoding is then used to compress the file. 

The general strategy behind Huffman codes is to scan through a given file and to use very few bits to encode frequently occurring characters, and allow for more bits to encode infrequently occurring characters. Each file will have its own Huffman Code that caters to the distribution of characters in the file. The representation, or �key�, of the character encoding for a particular file is stored in a Huffman Tree. A Huffman Tree is a binary tree with characters stored in leaf nodes. A path from the root to the leaf node generates the encoding of that particular character, with a traversal to a left node representing a 0, and a traversal to the right node representing a 1. The final Huffman Tree must be stored with the compressed file as a �key� used to decode the Huffman file back into its original form. 

My HuffmanCode.java creates Huffman codes for files containing ASCII characters. It can read and compress a file and also decode a compressed file and covert it back to its original ASCII characters.